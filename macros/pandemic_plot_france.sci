function pandemic_plot_france()
    // Plot per district for France data
    // https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/

    function [str, data] = get_data()
        fname = "63352e38-d353-4b54-bfd1-f1b3ee1cabd7";
        x = fileinfo(fullfile(TMPDIR, fname));
        if x == [] | getdate("s") - x(6) > 86400  then
            getURL("https://www.data.gouv.fr/fr/datasets/r/"+fname, TMPDIR);
            while fileinfo(fullfile(TMPDIR, fname)) == [] then sleep(100), end
        end

        str = csvRead(fullfile(TMPDIR, fname), ";", [], "string");
        data = csvRead(fullfile(TMPDIR, fname), ";");
    endfunction

    function M = movmean(A, k, varargin)
        select size(k)
        case [1 1] then
            kb = floor(k / 2);
            kf = kb;
        case [1 2] then
            kb = k(1);
            kf = k(2);
        else
            msg = "%s: Argument #%d: Scalar (1 element) expected.\n";
            error(msprintf(msg, "movmean", 2));
        end
        N = kb + kf +1;

        dim = 'm';
        if size(varargin) > 0 then
            dim = varargin(1);
        end

        if size(A, dim) < N then
            M = mean(A, dim);
            return
        end
        
        // $Mb = \sum_{k=1}^{kb} \frac{M_k}{N}$
        Mb = filter([1/(2*N) ; ones(kb,1)/N], 1, A($:-1:1))($:-1:1);
        
        // $Mf = \sum_{k=kf}^{\$} \frac{M_k}{N}$
        Mf = filter([1/(2*N) ; ones(kf,1)/N], 1, A);
        
        // $\overline M = Mb + Mf $
        M = Mb + Mf;
        
        // correct initial and ending points
        M(1:kb) = mean(A(1:kb+1));
        M($-kf+1:$) = mean(A($-kf:$));
    endfunction

    [str, data] = get_data();

    f = get("pandemic_plot_france_command_control");
    if f == [] then
        // init mode
        f = figure("background", -2, ...
        "default_axes", "off", ...
        "layout", "border", ...
        "figure_name", "pandemic_plot_france: COVID 19 cas par département", ...
        "menubar_visible", "off", ...
        "toolbar_visible", "off", ...
        "position", [0 0 200, 350], ...
        "visible", "off", ...
        "tag", "pandemic_plot_france_command_control");

        middle_frame = uicontrol(f, "style", "frame", ...
        "constraints", createConstraints("border", "center"), ...
        "layout", "gridbag");

        uicontrol(middle_frame, "style", "listbox", ...
        "callback", "pandemic_plot_france", ...
        "constraints", createConstraints("gridbag", [1 1 1 1], [0 0], "both"), ...
        "string", strcat(unique(str(2:$,1)), "|"), ...
        "tag", "departement", ...
        "value", 1);
        uicontrol(middle_frame, "style", "checkbox", ...
        "callback", "pandemic_plot_france", ...
        "constraints", createConstraints("gridbag", [1 2 1 1], [0 0], "horizontal"), ...
        "string", "hommes", ...
        "tag", "hommes", ...
        "value", 1);
        uicontrol(middle_frame, "style", "checkbox", ...
        "callback", "pandemic_plot_france", ...
        "constraints", createConstraints("gridbag", [1 3 1 1], [0 0], "horizontal"), ...
        "string", "femmes", ...
        "tag", "femmes", ...
        "value", 1);
        uicontrol(middle_frame, "style", "listbox", ...
        "callback", "pandemic_plot_france", ...
        "constraints", createConstraints("gridbag", [1 4 1 1], [0 0], "horizontal"), ...
        "string", strcat(str(1, 4:$), "|"), ...
        "tag", "numbers", ...
        "value", 1);
        
        f.visible = "on";
    end

    depIdx = get("pandemic_plot_france_command_control/*/departement", "value");
    depString = get("pandemic_plot_france_command_control/*/departement", "string");
    if depIdx == [] then
        return
    end

    row = str(:, 1) == depString(depIdx);
    catIdx = get("pandemic_plot_france_command_control/*/numbers", "value");
    if catIdx == [] then
        return
    end
    col = catIdx + 3;
    category = str(1, col);
    
    a = get(msprintf("pandemic_plot_france %s %s", depString(depIdx), category));
    if a == [] then
        f = figure("background", -2, "default_axes", "on", "figure_name", msprintf("pandemic_plot_france: COVID 19 département %s %s", depString(depIdx), category));
        f.resizefcn = "pandemic_plot_france_cb_resizefcn";
        a = f.children(1);
        a.tag = msprintf("pandemic_plot_france %s %s", depString(depIdx), category);
    else
        delete(a.children);
    end

    if get("pandemic_plot_france_command_control/*/hommes", "value") == 1 then
        d = data(row & str(:, 2) == "1", col);
    
        plot(a, d, "+b");
        plot(a, movmean(d, 7), "-r");
    end
    if get("pandemic_plot_france_command_control/*/femmes", "value") == 1 then
        d = data(row & str(:, 2) == "2", col);
    
        plot(a, d, "+b");
        plot(a, movmean(d, 7), "-g");
    end
    pandemic_plot_france_cb_resizefcn(f)
endfunction
