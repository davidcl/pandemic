function pandemic_plot_france_cb_resizefcn(gcbo)
    for a = gcbo.children(:)'
        if type(a) <> 9 | a.type <> "Axes" then
            continue;
        end
        
        a.auto_ticks(1) = "on";
        firstDateNum = datenum(2020, 03, 18);
        
        [Y,M,D] = datevec(firstDateNum + a.x_ticks.locations);
        a.x_ticks.labels = "$\rotatebox{30}{"+msprintf("%d-%02d-%02d\n", Y, M, D)+"}$";
    end
endfunction
