function f()
    [geoids, countries, timestamps, new_cases, new_deaths] = pandemic_covid_19_getdata();

    // All Countries
    scf(40); clf();
    pandemic_plot_new_deaths([], countries, timestamps, new_deaths);
endfunction
f()
clear f
