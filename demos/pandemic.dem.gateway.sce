//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()
    demopath = get_absolute_file_path("pandemic.dem.gateway.sce");

    subdemolist = [ "Covid19 Cases worldwide",      "covid_19_worldwide.dem.sce";
                    "Covid19 Cases in Europe",      "covid_19_europe.dem.sce";
                    "Covid19 Deaths worldwide",     "covid_19_deaths_worldwide.dem.sce";
                    "Covid19 Deaths in Europe",     "covid_19_deaths_europe.dem.sce";
                    "Covid19 New cases worldwide",  "covid_19_new_cases_worldwide.dem.sce";
                    "Covid19 New cases in Europe",  "covid_19_new_cases_europe.dem.sce";
                    "Covid19 New deaths worldwide", "covid_19_new_deaths_worldwide.dem.sce";
                    "Covid19 New deaths in Europe", "covid_19_new_deaths_europe.dem.sce"];

    subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
